from scrapy.item import Item
from scrapy.item import Field


class basicplatformItem(Item):
    url         = Field()
    regex_match = Field()
