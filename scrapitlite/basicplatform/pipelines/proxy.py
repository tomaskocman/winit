import json
import os
import random
import string
import subprocess
import sys

from apscheduler.executors.pool import ProcessPoolExecutor
from apscheduler.schedulers.twisted import TwistedScheduler
from scrapy.exceptions import NotConfigured
from scrapy.crawler import Crawler
from scrapy.spiders import Spider

from basicplatform.items import Item

executors = {
    'processpool': ProcessPoolExecutor(max_workers=4)
}
job_defaults = {
    'coalesce': True,
    'max_instancies': 4
}
scheduler = TwistedScheduler()
scheduler.configure(executors=executors, job_defaults=job_defaults)
scheduler.start()


class Proxy:
    def archive(self, url: str, output_dir: str):
        process = subprocess.Popen([f'{sys.exec_prefix}\\Scripts\\python.exe', 'pharty.py', '-u', url, '-o', output_dir])
        process.wait(timeout=10)

    def process_item(self, items: Item, spider: Spider):
        global shceduler
        items_json = dict(items)
        if items_json['regex_match'] != ['']:
            print(items_json['url'])
            os.chdir(f'{sys.exec_prefix}\\..\\lemmitlite')
            output_dir = 'output/' + ''.join(random.choices(string.ascii_letters + string.digits, k=16))
            scheduler.add_job(self.archive(items_json["url"], output_dir))

        return items
