import re

from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Response
from scrapy.linkextractors import LinkExtractor
from scrapy.utils.project import get_project_settings

from basicplatform.items import basicplatformItem


class BasicSpider(CrawlSpider):
    settings = get_project_settings()

    name = 'basic'
    allowed_domains = settings.getlist('ALLOWED_DOMAINS')
    start_urls = settings.getlist('START_URLS')

    pattern = re.compile(re.escape(settings.get('PATTERN')), re.IGNORECASE | re.MULTILINE)

    # Rules for crawling.
    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )

    def parse_item(self, response: Response):
        # Create the item loader using the response
        loader = ItemLoader(item=basicplatformItem(), response=response)
        loader.add_value('url', response.url)

        matches = set(re.findall(pattern=BasicSpider.pattern, string=response.text))
        matches_str = ''
        for match in matches:
            matches_str += match + ';'

        loader.add_value('regex_match', matches_str)

        return loader.load_item()
