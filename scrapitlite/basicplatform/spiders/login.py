import datetime
import socket
import re

from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Response, FormRequest, Request
from scrapy.linkextractors import LinkExtractor
from scrapy.utils.project import get_project_settings

from basicplatform.items import basicplatformItem


class BasicSpider(CrawlSpider):
    """Spider class for scraping/crawling.
    Spider supports login (servers requiring authentication).
    If regex matches, save the matches.
    """
    settings = get_project_settings()

    name = 'login'
    allowed_domains = settings.getlist('ALLOWED_DOMAINS')
    start_urls = settings.getlist('START_URLS')

    pattern = re.compile(re.escape(settings.get('PATTERN')), re.IGNORECASE | re.MULTILINE)

    # Rules for crawling.
    rules = (
        Rule(LinkExtractor(allow=r''), callback='parse_item', follow=True),
    )

    def start_requests(self):
        """First http request has to be login request.
        Scrapy calls this method before any subsequent request.
        """
        return [
            Request(
                url=BasicSpider.settings.get('LOGIN_URL'),
                callback=self.login,
                dont_filter=True
            )
        ]

    def login(self, response):
        """Logs in using all necessary form fields with login and password.

        Args:
            response: Http response from initial request.
        """
        return FormRequest.from_response(
            response,
            formname=BasicSpider.settings.get('FORM_NAME'),
            formdata={
                BasicSpider.settings.get('FORM_LOGIN')   : self.username,
                BasicSpider.settings.get('FORM_PASSWORD'): self.password
            },
            callback=self.check_login_response
        )

    def check_login_response(self, response):
        """
        
        :param response: 
        :return: 
        """
        for url in BasicSpider.start_urls:
            yield Request(url=url)

    def parse_item(self, response: Response) -> ItemLoader:
        """This function is called as callback to parse each scraped page.

        Args:
            response: Scraped page.

        Returns:
            Istance of item loader with all filled fields.
        """
        # Create the item loader using the response.
        loader = ItemLoader(item=BasicplatformItem(), response=response)

        # Housekeeping fields.
        loader.add_value('url'   , response.url)
        loader.add_value('job'   , self.settings.get('JOB'))
        loader.add_value('spider', self.name)
        loader.add_value('server', socket.gethostname())
        loader.add_value('date'  , datetime.datetime.now().isoformat(' '))

        # Primary fields.
        loader.add_value('headers'    , str(response.headers))
        loader.add_value('htmlcontent', str(response.text))

        matches = set(re.findall(pattern=BasicSpider.pattern, string=response.text))
        matches_str = ''
        for match in matches:
            matches_str += match + ';'

        loader.add_value('regex_match', matches_str)
        loader.add_value('regex_pattern', get_project_settings().get('PATTERN'))

        return loader.load_item()
