# Winit
![Test Image 1](logo.png)<br/>
<br/>
#### Authors: Bc. Tomáš Kocman, Ing. Libor Polčák, Ph.D.<br/>
This is a standalone application supporting Windows systems.<br/>

This tool allows web page content scrapping and exporting the content as a compressed archive.
The web crawl is performed using user-supplied regular expressions that may represent for example
Torrent file names, Bitcoin wallets or keywords. Collected data may be used for law enforcement
and other entitites, such as searching for information about a specific product. The tool can be
used in Microsoft Windows without external dependecies.

Scrapy serves as a web crawler/scraper. It crawls through the whole web and analyzes individual pages.<br/>
**Input for crawling:** informations in settings.py.<br/>
**Output of crawling:** JSON objects stored in Redis database.

Lemmiwinks serves for web page archivation. It recursively downloads all resources found on the page.<br/>
**Input for archivation:** JSON objects from Redis database.<br/>
**Output of archivation:** MAFF archive with metainformations stored in PostgreSQL database.

### Steps to run Scrapy
```
> Create and activate Python virtual environment
> Ensure C++ build tools are installed in order to compile Python binary dependencies
> Install dependencies from requirements.txt
> cd scrapitlite; scrapy crawl basic
```

### Acknowledgments

This work was supported by the Ministry of the Interior of the Czech Republic grant
number VI20172020062.